<?php

/**
 * INIT: Register Child Theme (CSS)
 */

function my_theme_enqueue_styles()
{
    $parent_style = 'twentysixteen-style';

    wp_enqueue_style($parent_style, get_template_directory_uri() . '/style.css');
    wp_enqueue_style('child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array($parent_style),
        wp_get_theme()->get('Version')
    );
}

add_action('wp_enqueue_scripts', 'my_theme_enqueue_styles');

/**
 * ============================================================================
 * Flush rewrite-rules to prevent 404
 * @author Matthieu FAURE <mfaure@asqatasun.org>
 * @See http://www.wpexplorer.com/post-type-404-error/
 */
function my_flush_rewrite_rules()
{
    flush_rewrite_rules();
}

add_action('after_switch_theme', 'my_flush_rewrite_rules');

/**
 * Custom Post Type : Fiche lecture
 * @author Matthieu FAURE <mfaure@asqatasun.org>
 */

if (!function_exists('cdmeb_ci_doc')) {
// Register Custom Post Type
    function cdmeb_ci_doc()
    {
        $labels = array(
            'name' => _x('Fiche lecture', 'Post Type General Name', 'cdmeb'),
            'singular_name' => _x('Fiche lecture', 'Post Type Singular Name', 'cdmeb'),
            'menu_name' => __('Fiches lecture', 'cdmeb'),
            'name_admin_bar' => __('Fiche Lecture', 'cdmeb'),
            'archives' => __('Fiche Lecture Archives', 'cdmeb'),
            'attributes' => __('Fiche Lecture Attributes', 'cdmeb'),
            'parent_item_colon' => __('', 'cdmeb'),
            'all_items' => __('All Fiches lecture', 'cdmeb'),
            'add_new_item' => __('Add Fiche Lecture', 'cdmeb'),
            'add_new' => __('Add New Fiche Lecture', 'cdmeb'),
            'new_item' => __('New Fiche Lecture', 'cdmeb'),
            'edit_item' => __('Edit Fiche Lecture', 'cdmeb'),
            'update_item' => __('Update Fiche Lecture', 'cdmeb'),
            'view_item' => __('View Fiche Lecture', 'cdmeb'),
            'view_items' => __('View Fiches lecture', 'cdmeb'),
            'search_items' => __('Search Fiche Lecture', 'cdmeb'),
            'not_found' => __('Not found', 'cdmeb'),
            'not_found_in_trash' => __('Not found in Trash', 'cdmeb'),
            'featured_image' => __('Featured Image', 'cdmeb'),
            'set_featured_image' => __('Set featured image', 'cdmeb'),
            'remove_featured_image' => __('Remove featured image', 'cdmeb'),
            'use_featured_image' => __('Use as featured image', 'cdmeb'),
            'insert_into_item' => __('Insert into Fiche Lecture', 'cdmeb'),
            'uploaded_to_this_item' => __('Uploaded to this Fiche Lecture', 'cdmeb'),
            'items_list' => __('Fiches lecture list', 'cdmeb'),
            'items_list_navigation' => __('Fiches lecture list navigation', 'cdmeb'),
            'filter_items_list' => __('Filter Fiches lecture list', 'cdmeb'),
        );
        $args = array(
            'label' => __('Fiche lecture', 'cdmeb'),
            'description' => __('Identity Card of Document', 'cdmeb'),
            'labels' => $labels,
            'supports' => array('title', 'editor', 'thumbnail', 'comments', 'revisions', 'custom-fields', 'page-attributes', 'post-formats',),
            'taxonomies' => array('category', 'post_tag'),
            'hierarchical' => false,
            'public' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'menu_position' => 3,
            'menu_icon' => 'dashicons-book',
            'show_in_admin_bar' => true,
            'show_in_nav_menus' => true,
            'can_export' => true,
            'has_archive' => true,
            'exclude_from_search' => false,
            'publicly_queryable' => true,
            'capability_type' => 'post',
        );
        register_post_type('fiche-lecture', $args);
    }

    add_action('init', 'cdmeb_ci_doc', 0);
}

/**
 * ============================================================================
 * Add Fiche Lecture to "visible" documents depending on template
 * @author Matthieu FAURE <mfaure@asqatasun.org>
 * @see https://wpchannel.com/custom-post-types-pages-auteurs-wordpress/
 * @see https://codex.wordpress.org/Plugin_API/Action_Reference/pre_get_posts#Include_Custom_Post_Types_in_Search_Results
 * @see https://developer.wordpress.org/files/2014/10/template-hierarchy.png
 * @see https://codex.wordpress.org/Conditional_Tags
 */
function add_my_post_types_to_query($query)
{
    if (!is_admin() && $query->is_main_query()) {
        if (is_home() || is_archive() || is_single() || is_author())
            $query->set('post_type', array('post', 'page', 'fiche-lecture'));
    }
    return $query;
}

add_action('pre_get_posts', 'add_my_post_types_to_query');

/**
 * ============================================================================
 * This function overrides the same one from the parent theme.
 * It adds the metadata of a fiche-lecture (author, contact, date)
 * @author Matthieu FAURE <mfaure@asqatasun.org>
 */
if (!function_exists('twentysixteen_entry_meta')) :
    /**
     * Prints HTML with meta information for the categories, tags.
     *
     * Create your own twentysixteen_entry_meta() function to override in a child theme.
     *
     * @since Twenty Sixteen 1.0
     */
    function twentysixteen_entry_meta()
    {
        if ('post' === get_post_type()) {
            $author_avatar_size = apply_filters('twentysixteen_author_avatar_size', 49);
            printf('<span class="byline"><span class="author vcard">%1$s<span class="screen-reader-text">%2$s </span> <a class="url fn n" href="%3$s">%4$s</a></span></span>',
                get_avatar(get_the_author_meta('user_email'), $author_avatar_size),
                _x('Author', 'Used before post author name.', 'twentysixteen'),
                esc_url(get_author_posts_url(get_the_author_meta('ID'))),
                get_the_author()
            );
        }

        // **THIS** is one of the added part
        if ('fiche-lecture' === get_post_type()) {
            $tag_official_doc = "";
            $my_date = "";

            // Prepare special print for tag "Glénans_OFFICIEL" (id = 7)
            if (has_tag('7')) {
                $tag_official_doc = '<p><span class="glenans-officiel">Document Officiel</span></p>';
            }

            // Prepare date for fiche-lecture
            if (get_field('document_date')) {
                // Prepare date format as "3 février 2017"
                setlocale(LC_ALL, 'fr_FR.UTF-8');
                $tz_paris = new DateTimeZone('Europe/Paris');
                $my_timestamp = new DateTime(get_field('document_date'), $tz_paris);
                $my_date = strftime("%e %B %Y", $my_timestamp->getTimestamp());
            }

            if ($tag_official_doc != "") {
                echo "$tag_official_doc";
            }

            if ($my_date) {
                printf('<p class="fiche-lecture-date">%1$s</p>',
                    $my_date
                );
            }

            // Prepare author name
            if (get_field('author_name') && get_field('author_email')) {
                printf('<p class="fiche-lecture-auteur">%1$s <br /><code>%2$s</code></p>',
                    get_field('author_name'),
                    get_field('author_email')
                );
            }
        }

        if (in_array(get_post_type(), array('post', 'attachment'))) {
            twentysixteen_entry_date();
        }

        $format = get_post_format();
        if (current_theme_supports('post-formats', $format)) {
            printf('<span class="entry-format">%1$s<a href="%2$s">%3$s</a></span>',
                sprintf('<span class="screen-reader-text">%s </span>', _x('Format', 'Used before post format.', 'twentysixteen')),
                esc_url(get_post_format_link($format)),
                get_post_format_string($format)
            );
        }

        // **THIS** is one of the modified  part
        if ('post' === get_post_type() || 'fiche-lecture' === get_post_type()) {
            twentysixteen_entry_taxonomies();
        }

        if (!is_singular() && !post_password_required() && (comments_open() || get_comments_number())) {
            echo '<span class="comments-link">';
            comments_popup_link(sprintf(__('Leave a comment<span class="screen-reader-text"> on %s</span>', 'twentysixteen'), get_the_title()));
            echo '</span>';
        }

    }
endif;

/**
 * ============================================================================
 * This function overrides the same one from the parent theme.
 * It backports some SVG-icons from Twenty-Seventeen
 * @author Matthieu FAURE <mfaure@asqatasun.org>
 */
if (!function_exists('twentysixteen_entry_taxonomies')) :
    function twentysixteen_entry_taxonomies()
    {
        $categories_list = get_the_category_list(_x(', ', 'Used between list items, there is a space after the comma.', 'twentysixteen'));
        if ($categories_list && twentysixteen_categorized_blog()) {
            echo '<p class="cat-links">';
            echo '<svg class="svg-icon" aria-hidden="true" role="img" viewBox="0 0 34 32"><path class="path1" d="M33.554 17q0 0.554-0.554 1.179l-6 7.071q-0.768 0.911-2.152 1.545t-2.563 0.634h-19.429q-0.607 0-1.080-0.232t-0.473-0.768q0-0.554 0.554-1.179l6-7.071q0.768-0.911 2.152-1.545t2.563-0.634h19.429q0.607 0 1.080 0.232t0.473 0.768zM27.429 10.857v2.857h-14.857q-1.679 0-3.518 0.848t-2.929 2.134l-6.107 7.179q0-0.071-0.009-0.223t-0.009-0.223v-17.143q0-1.643 1.179-2.821t2.821-1.179h5.714q1.643 0 2.821 1.179t1.179 2.821v0.571h9.714q1.643 0 2.821 1.179t1.179 2.821z"></path></svg>';
            printf('<span class="screen-reader-text">%1$s</span>%2$s',
                _x('Categories', 'Used before category names.', 'twentysixteen'),
                $categories_list
            );
            echo '</p>';
        }

        $tags_list = get_the_tag_list('', _x(', ', 'Used between list items, there is a space after the comma.', 'twentysixteen'));
        if ($tags_list) {
            echo '<p class="tags-links">';
            echo '<svg class="svg-icon" aria-hidden="true" role="img" viewBox="0 0 32 32"><path class="path1" d="M17.696 18.286l1.143-4.571h-4.536l-1.143 4.571h4.536zM31.411 9.286l-1 4q-0.125 0.429-0.554 0.429h-5.839l-1.143 4.571h5.554q0.268 0 0.446 0.214 0.179 0.25 0.107 0.5l-1 4q-0.089 0.429-0.554 0.429h-5.839l-1.446 5.857q-0.125 0.429-0.554 0.429h-4q-0.286 0-0.464-0.214-0.161-0.214-0.107-0.5l1.393-5.571h-4.536l-1.446 5.857q-0.125 0.429-0.554 0.429h-4.018q-0.268 0-0.446-0.214-0.161-0.214-0.107-0.5l1.393-5.571h-5.554q-0.268 0-0.446-0.214-0.161-0.214-0.107-0.5l1-4q0.125-0.429 0.554-0.429h5.839l1.143-4.571h-5.554q-0.268 0-0.446-0.214-0.179-0.25-0.107-0.5l1-4q0.089-0.429 0.554-0.429h5.839l1.446-5.857q0.125-0.429 0.571-0.429h4q0.268 0 0.446 0.214 0.161 0.214 0.107 0.5l-1.393 5.571h4.536l1.446-5.857q0.125-0.429 0.571-0.429h4q0.268 0 0.446 0.214 0.161 0.214 0.107 0.5l-1.393 5.571h5.554q0.268 0 0.446 0.214 0.161 0.214 0.107 0.5z"></path></svg>';
            printf('<span class="screen-reader-text">%1$s </span>%2$s',
                _x('Tags', 'Used before tag names.', 'twentysixteen'),
                $tags_list
            );
            echo '</p>';
        }
    }
endif;
